from pyglet.gl import *
from pyglet.window import key
from pyglet.window import mouse


class PolygonElement(object):
    # vertex = (x, y)
    def __init__(self, vertex):
        self.vertex = vertex

width = 300
height = 300

# otvara prozor
config = pyglet.gl.Config(double_buffer=False)
window = pyglet.window.Window(width=width, height=height, caption="Polygon", resizable=False, config=config, visible=False)
window.set_location(100, 100)

isDrawEnabled=True
points=[]
testPoint=None
normalPoint=True    # False for test point

def getPolygonElements():
    polyElems = []
    n = len(points)
    for i in range(n):
        polyElems.append(PolygonElement(points[i]))
    return polyElems



def drawPolygon():
    glColor3f(0, 0, 0)

    n = len(points)
    i0 = n-1

    for i in range(n):
        glBegin(GL_LINES)
        glVertex2i(points[i0][0], points[i0][1])
        glVertex2i(points[i][0], points[i][1])
        glEnd()
        i0 = i

def calcEdgeCoeffs(polyElems):
    n = len(points)
    i0 = n-1

    for i in range(n):
        a = polyElems[i0].vertex[1] - polyElems[i].vertex[1]
        b = -(polyElems[i0].vertex[0] - polyElems[i].vertex[0])
        c = polyElems[i0].vertex[0] * polyElems[i].vertex[1] - polyElems[i0].vertex[1] * polyElems[i].vertex[0]
        polyElems[i0].edge = (a, b, c)
        polyElems[i0].left = polyElems[i0].vertex[1] < polyElems[i].vertex[1]
        i0 = i

    return polyElems

def checkTestPointWithinPolygon(polyElems):
    #print("test=",testPoint)

    for polyEl in polyElems:
        a, b, c = polyEl.edge
        if (a * testPoint[0] + b * testPoint[1] + c) > 0:
            return False

    return True

def paintPolygon(polyElems):
    glColor3f(0, 0, 1)

    n = len(polyElems)

    xCoords = [point[0] for point in points]
    yCoords = [point[1] for point in points]
    xmin, xmax, ymin, ymax = min(xCoords), max(xCoords), min(yCoords), max(yCoords)
    #print(xmin, xmax, ymin, ymax)

    for y in range(ymin, ymax+1):
        L, D = xmin, xmax

        i0 = n - 1
        last_i = None
        for i in range(n):
            if last_i is not None:
                i0 = last_i
                last_i += 1
            else:
                last_i = i

            # edge horizontal
            if polyElems[i0].edge[0] == 0:
                # edge exactly on current y line being drawn
                if polyElems[i0].vertex[1] == y:
                    if polyElems[i0].vertex[0] < polyElems[i].vertex[0]:
                        L, D = polyElems[i0].vertex[0], polyElems[i].vertex[0]
                    else:
                        L, D = polyElems[i].vertex[0], polyElems[i0].vertex[0]
                    break
            else:
                x = (-polyElems[i0].edge[1]*y-polyElems[i0].edge[2]) / polyElems[i0].edge[0]
                if polyElems[i0].left:
                    if L < x:
                        L = x
                else:
                    if D > x:
                        D = x
        glBegin(GL_LINES)
        glVertex2i(round(L), y)
        glVertex2i(round(D), y)
        glEnd()

def calcAndDrawPolygon():
    polyElems = getPolygonElements()

    polyElems = calcEdgeCoeffs(polyElems)

    isConvex, isCWOrientation = checkPolygonConvexAndOrient(polyElems)

    global points
    if not isConvex:
        print("Zadani poligon nije konveksan. Molim zadajte tocke ponovo.")
        global vertNum
        vertNum = enteredNum
        global normalPoint
        normalPoint = True
        points = []
        window.clear()
        return

    if not isCWOrientation:
        print("Zadane su tocke u smjeru suprotnom od kazaljke na satu. Okrecem orijentaciju i preracunavam...")
        points = points[::-1]
        polyElems = getPolygonElements()
        polyElems = calcEdgeCoeffs(polyElems)

    #drawPolygon()
    isPointInPolygon = checkTestPointWithinPolygon(polyElems)
    if isPointInPolygon:
        print("Tocka V je unutar poligona!")
    else:
        print("Tocka V je IZVAN poligona!")

    paintPolygon(polyElems)

def checkPolygonConvexAndOrient(polyElems):
    n = len(polyElems)

    below = above = 0
    i0 = n-2
    for i in range(n):
        if i0 >= n:
            i0 = 0
        r = polyElems[i0].edge[0]*polyElems[i].vertex[0] + polyElems[i0].edge[1]*polyElems[i].vertex[1] + polyElems[i0].edge[2]
        if r > 0:
            above += 1
        elif r < 0:
            below += 1
        i0 += 1

    isCWOrientation = None
    if below == 0:
        isConvex = True
        isCWOrientation = False
    elif above == 0:
        isConvex = True
        isCWOrientation = True
    else:
        isConvex = False

    return isConvex, isCWOrientation


@window.event
def on_mouse_press(x, y, button, modifiers):
    global vertNum
    if vertNum == 0:
        return

    if button & mouse.LEFT:

        global normalPoint
        if normalPoint:
            if (x, y) in points:
                print("Tocka je vec izabrana. Molim odaberite drugu.")
                return

            glColor3f(1, 0, 0)
            glBegin(GL_POINTS)
            glVertex2i(x, y)
            glEnd()

            points.append((x, y))
            vertNum -= 1
            print("Koordinate tocke: {} {} ".format(x, y))

            if vertNum == 0:
                drawPolygon()
                normalPoint = False
                vertNum = 1
                print("Odaberite ispitnu tocku...")

            glFlush()
        else:
            glColor3f(0, 1, 0)
            glBegin(GL_POINTS)
            glVertex2i(x, y)
            glEnd()

            global testPoint
            testPoint = (x, y)
            vertNum = 0
            print("Koordinate ispitne tocke: {} {} ".format(x, y))

            glFlush()

            calcAndDrawPolygon()

@window.event
def on_draw():
    glFlush()

@window.event
def on_resize(w, h):
    global width, height
    width = w
    height = h

    glViewport(0, 0, width, height)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0, width, 0, height)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glPointSize(3.0)    # velicina 3 da se bolje vidi tocka
    glColor3f(0.0, 0.0, 0.0)


def enterVertexNumber():
    vertNum = -1
    try:
        vertNum = int(input("Broj vrhova poligona: "))
    except ValueError:
        print("Broj vrhova treba biti cijeli broj.")

    if vertNum < 3 and vertNum != -1:
        print("Broj vrhova treba biti cijeli broj >=3.")

    while vertNum < 3:
        try:
            vertNum = int(input("Broj vrhova poligona: "))
        except ValueError:
            print("Broj vrhova treba biti cijeli broj.")
        else:
            if vertNum >= 3:
                return vertNum
            print("Broj vrhova treba biti cijeli broj >=3.")

    return vertNum

if __name__ == "__main__":

    vertNum = enterVertexNumber()
    enteredNum = vertNum

    window.set_visible(True)

    pyglet.app.run()