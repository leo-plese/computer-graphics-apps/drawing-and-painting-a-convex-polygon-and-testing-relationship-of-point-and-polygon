Drawing and Painting a Convex Polygon and Testing Relationship of Point and Polygon. Implemented in Python using pyglet, Python OpenGL interface.

My lab assignment in Interactive Computer Graphics, FER, Zagreb.

Docs in "DokumentacijaIRGLabosi" under "3. VJEZBA."

Created: 2020
